class PointsPlugin:
    def __init__(self):
        self.passed_tests = set()
        self.failed_tests = set()

    def pytest_report_teststatus(self, report):
        weight = None
        for prop, val in report.user_properties:
            if prop == "weight":
                weight = val
                break

        if not weight:
            return

        if report.passed:
            self.passed_tests.add((report.nodeid, weight))
        else:
            self.failed_tests.add((report.nodeid, weight))

        self.passed_tests -= self.failed_tests

    @property
    def possible_points(self):
        ret = 0

        for _, weight in self.passed_tests:
            ret += weight

        for _, weight in self.failed_tests:
            ret += weight

        return ret

    @property
    def awarded_points(self):
        ret = 0

        for _, weight in self.passed_tests:
            ret += weight

        return ret

    def score(self):
        return self.awarded_points / self.possible_points
